#!/bin/bash

DIR_PATH=$(dirname $(readlink -f $0))

# Create folder
# usage: create_folder foler1 folder2 folder3...
#    ie: create_folder /folder/path/you/want /foler/path/you/want2
create_folder() {
    for var in "$@" ; do
        if [ ! -d $var ] ; then
            mkdir -p $var
        fi
    done    
}

# Create dhparam key
create_dhparam_key() {
    if [ ! -f $DIR_PATH/src/ssl/dhparams.pem ] ; then
        echo "Create dhparams key, please watting..."
        openssl dhparam -out $DIR_PATH/src/ssl/dhparams.pem 2048
    else
        echo -n "Dhparams key exist, do you want re-create? (y/N): "
        read answer
        if [ "$answer" == "Y" ] || [ "$answer" == "y" ] ; then
            echo "Re-Create dhparams key, please watting..."
            openssl dhparam -out $DIR_PATH/src/ssl/dhparams.pem 4096
        fi
    fi

}

main() {
    #if [ ! -f $DIR_PATH/files/ssl/dhparams.pem ] ; then
    #    openssl dhparam -out $DIR_PATH/files/ssl/dhparams.pem 4096
    #fi
    create_folder $DIR_PATH/log
    create_dhparam_key
    
    if [ ! -f "$DIR_PATH/src/ssl/ca.key" ] && [ ! -f "$DIR_PATH/src/ssl/ca.crt" ] ; then
        echo "Put your CA to \"$DIR_PATH/src/ssl/\", and named \"ca.key & ca.crt\""
    fi
}

main "$@"
