#!/bin/sh

if [ -f "/etc/nginx/ssl/ca.crt" ] && [ -f "/etc/nginx/ssl/ca.key" ] ; then
    cp /etc/nginx/conf.d/registry_with_ssl.default /etc/nginx/conf.d/registry.conf

    sed -i "s/proxy_pass http:/proxy_pass https:/g" /etc/nginx/conf.d/registry.conf
else
    cp /etc/nginx/conf.d/registry_without_ssl.default /etc/nginx/conf.d/registry.conf

    sed -i "s/proxy_pass https:/proxy_pass http:/g" /etc/nginx/conf.d/registry.conf
fi

sed -i "s,\${URL},${URL}," scripts/script.js
sed -i "s,\${REGISTRY_TITLE},${REGISTRY_TITLE}," scripts/script.js

if [ -z "${DELETE_IMAGES}" ] || [ "${DELETE_IMAGES}" = false ] ; then
    sed -i "s/registryUI.isImageRemoveActivated *= *[^,;]*/registryUI.isImageRemoveActivated=false/" scripts/script.js
fi

#if [ -f "/passwd/.htpasswd" ] ; then
#    :
#fi
#if [ -n "${REGISTRY_URL}" ] ; then
#    sed -i "s#proxy_pass http.*#proxy_pass $REGISTRY_URL;#g" /etc/nginx/conf.d/registry
#    sed -i "s,#!,," /etc/nginx/conf.d/registry
#fi

#service nginx restart
#exec /sbin/init
exec /usr/sbin/nginx -g 'daemon off;'
