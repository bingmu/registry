#!/bin/sh

if [ ! -f /passwd/.htpasswd ] ; then
    htpasswd -cbB /passwd/.htpasswd admin p@ssw0rd
fi

exec /sbin/init
