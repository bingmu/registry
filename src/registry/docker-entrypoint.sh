#!/bin/sh

set -e

  #tls:
  #  certificate: /etc/ssl/ca.crt
  #  key: /etc/ssl/ca.key
if [ -f "/etc/ssl/ca.crt" ] && [ -f "/etc/ssl/ca.key" ] ; then
    sed -i "s^#tls:$^tls:^g" /etc/docker/registry/config.yml
    sed -i "s^#  certificate: /etc/ssl/ca.crt^  certificate: /etc/ssl/ca.crt^g" /etc/docker/registry/config.yml
    sed -i "s^#  key: /etc/ssl/ca.key^  key: /etc/ssl/ca.key^g" /etc/docker/registry/config.yml
else
    sed -i "s^  tls:$^  #tls:^g" /etc/docker/registry/config.yml
    sed -i "s^    certificate: /etc/ssl/ca.crt^  #  certificate: /etc/ssl/ca.crt^g" /etc/docker/registry/config.yml
    sed -i "s^    key: /etc/ssl/ca.key^  #  key: /etc/ssl/ca.key^g" /etc/docker/registry/config.yml
fi

case "$1" in
    *.yaml|*.yml) set -- registry serve "$@" ;;
    serve|garbage-collect|help|-*) set -- registry "$@" ;;
esac

exec "$@"
