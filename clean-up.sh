#!/bin/bash

docker exec -ti registry registry garbage-collect /etc/docker/registry/config.yml
docker-compose restart registry
